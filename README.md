
ShinyESS
========

Overview
--------

ShinyESS contains a Shiny application to supplement the material in Elementary Survey Sampling 7th edition by Scheaffer et al. The applications provide a simple graphical user interface for users to solve basic survey sampling problems with R.

**Note: This package is still the development stage, so expect limited functionality.**

Installation
------------

-   Install R. Skip if already installed.

             <https://cran.r-project.org/bin/windows/base/>

-   Install RStudio (not required, but highly recommended). Skip if already installed.

             <https://www.rstudio.com/products/rstudio/download/>

-   Install devtools. Skip if already installed.

``` r
install.packages("devtools")
```

-   Install ShinyESS.

``` r
devtools::install_bitbucket("ebrist/ShinyESS", upgrade = F)
```

**Reason for not upgrading dependencies:**

Older versions of certain dependencies are already installed on the C Drive for University of Wyoming computers. This is helpful since space is limited on the H Drive. However, if newer versions of dependencies are available, the newer versions will be installed to the H Drive if upgrade = T. This will take much longer and enough space may not be available. The older versions on the C Drive will work fine, so it is recommended that dependencies are not upgraded.

Usage
-----

Once the package is installed, load the package into memory.

``` r
library(ShinyESS)
```

Use `runESS()` to open the Shiny application.

``` r
runESS()
```

Alternatively, the user could cd into the application directory and run `shiny::runApp()`.

Notes
-----

1.  The application is much easier to use in full-screen mode.

2.  Chrome is strongly recommended when opening this application in an internet browser. Some browsers such as Internet Explorer will not display the application correctly.
