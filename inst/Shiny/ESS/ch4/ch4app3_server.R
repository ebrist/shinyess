### ch4app3 server ###

# inputs ----

# text data options
dataset_choices43 <- eval(parse(file = "datasets/data_list.R"))
# text data options 
datasetInput43a <- reactive({
  req(input$type43, input$dataset43)
  switch(input$dataset43, eval(parse(text = input$dataset43)))
}) 
# input file
datasetInput43b <- reactive({
  req(input$type43, input$file43)
  read.csv(input$file43$datapath, header = ifelse(input$header43 == "yes", TRUE, FALSE), sep = input$sep43)
})
# switch data
data43 <- reactive({
  req(datasetInput43a, datasetInput43b)
  if (input$type43 == "user_data43") {
    datasetInput43b()
  } else {
    datasetInput43a()
  }
})
# select the dataset 
output$select_dataset43 <- renderUI({
 selectInput("dataset43", "Choose a Dataset", dataset_choices43, selected = "ShinyESS::data4.41")
})
# select .csv file 
output$select_csv43 <- renderUI({
  fileInput("file43", "Choose a .CSV File", multiple = TRUE, 
            accept = c("text/csv", "text/comma-separated-values,text/plain", ".csv"))
})
# select text variable
output$select_y43a <- renderUI({
  req(data43())
  selectInput("yvar43a", "Choose a Variable", names(data43()), selected = "Compliance")
})
# select user data variable 
output$select_y43b <- renderUI({
  req(data43())
  selectInput("yvar43b", "Choose a Variable", names(data43()))
})
# input select category (text data) 
output$select_cat43a <- renderUI({
  req(input$yvar43a, data43())
  selectInput("cat43a", "Choose a Category", data43()[[input$yvar43a]] %>% as.factor() %>% levels())
})
# input select category (user data)
output$select_cat43b <- renderUI({
  req(input$yvar43b, data43())
  selectInput("cat43b", "Choose a Category", data43()[[input$yvar43b]] %>% as.factor() %>% levels())
})
# input population total (text data) 
output$select_N43a <- renderUI({
  req(data43())
  numericInput("popsize43a", "Population Size", min = nrow(data43()), max = Inf, value = NULL)
})
# input population total (user data) 
output$select_N43b <- renderUI({
  req(data43())
  numericInput("popsize43b", "Population Size", min = nrow(data43()), max = Inf, value = NULL)
})
# input proportion 
output$select_prop43c <- renderUI({
  numericInput("prop43c", "Sample Proportion", min = 0, max = 1, value = NULL)
})
# input sample size 
output$select_n43c <- renderUI({
  numericInput("n43c", "Sample Size", min = 2, max = Inf, value = NULL)
})
# input population size
output$select_N43c <- renderUI({
  req(input$n43c)
  numericInput("N43c", "Population Size", min = 3, max = Inf, value = NULL)
})

# output options ----

# fpc
output$fpc43 <- renderUI({
  radioButtons("fpc43", "FPC", c(Yes = "yes", No = "no"), selected = "yes", inline = T)
})

# multiplier
output$mult43 <- renderUI({
  numericInput("mult43", "Interval Multiplier", value = 2, min = 0, max = Inf)
})
# confidence level 
output$conf43 <- renderUI({
  sliderInput("conf43", "Confidence Level", value = 95, min = 50, max = 99, ticks = F, post = "%")
})
# method 
output$method43 <- renderUI({
  radioButtons("method43", "Interval Method", 
               c("Standard Asymptotic" = "stand43", "Agresti-Coull" = "ac43"), selected = "stand43")
})
# rmoe 
output$checkrmoe43 <- renderUI({
  radioButtons("checkrmoe43", "RMoE", c(Yes = "yes", No = "no"), selected = "no", inline = T)
})
# interval
output$checkci43 <- renderUI({
  radioButtons("checkci43", "Interval", c(Yes = "yes", No = "no"),selected = "yes", inline = T)
})
# number of digits to display 
output$digits43 <- renderUI({
  sliderInput("digits43", "Number of Decimals", value = 2, min = 0, max = 10, ticks = F)
})
# preview data
output$data_table43 <- DT::renderDataTable({
  req(input$type43)
  data <- data43()
  if (input$type43 == "user_values43") {data <- NULL} 
  else {
    req(data43())
    DT::datatable(data, rownames = F, style = 'bootstrap', 
                  options = list(columnDefs = list(list(className = 'dt-right', targets = "_all")))) %>% 
      DT::formatStyle(columns = 1:ncol(data), fontSize = '12pt')}})

# output table ----

output$output_table43 <- function() {
  req(input$digits43, data43(), input$type43, input$method43, input$fpc43)
  show_rmoe <- ifelse(input$checkrmoe43 == 'yes', TRUE, FALSE)
  show_ci <- ifelse(input$checkci43 == 'yes', TRUE, FALSE)
  if (input$type43 == "text_data43") {
    req(input$cat43a, input$yvar43a, input$popsize43a)
    p_hat <- mean(na.omit(as.numeric(data43()[[input$yvar43a]] == input$cat43a)))
    n <- length(na.omit(as.numeric(data43()[[input$yvar43a]])))
    N <- input$popsize43a
  } else if (input$type43 == "user_data43") {
    req(input$cat43b, input$yvar43b, input$popsize43b)
    p_hat <- mean(na.omit(as.numeric(data43()[[input$yvar43b]] == input$cat43b)))
    n <- length(na.omit(as.numeric(data43()[[input$yvar43b]])))
    N <- input$popsize43b
  } else {
    req(input$prop43c, input$n43c, input$N43c)
    p_hat <- input$prop43c
    n <- input$n43c
    N <- input$N43c
  } 
  fpc <- ifelse(input$fpc43 == "yes", 1 - n / N, 1)
  # ac interval
  if (input$method43 == "ac43") {
    req(input$conf43)
    alpha <- (0.5 - input$conf43 / 200)
    z <- qnorm(1 - alpha)
    z2 <- z * z
    x_adj <- p_hat * n + 0.5 * z2
    n_adj <- n + z2
    p_hat_adj <- x_adj / n_adj
    sd_prop <- sqrt(fpc * (p_hat_adj * (1 - p_hat_adj)) / (n_adj))
    moe_prop <- z * sd_prop
    ci_lwr_prop <- max(p_hat_adj - moe_prop, 0)
    ci_upr_prop <- min(p_hat_adj + moe_prop, 1)
    ci_lwr_total <- ci_lwr_prop * N
    ci_upr_total <- ci_upr_prop * N} 
  # standard interval
  if (input$method43 == "stand43") {
    req(input$mult43)
    sd_prop <- sqrt(fpc * (p_hat * (1 - p_hat)) / (n - 1))
    moe_prop <- input$mult43 * sd_prop
    ci_lwr_prop <- max(p_hat - moe_prop, 0); ci_upr_prop <- min(p_hat + moe_prop, 1)
    ci_lwr_total <- ci_lwr_prop * N; ci_upr_total <- ci_upr_prop * N}
  # total
  total <- p_hat * N
  sd_total <- sqrt(N^2 * fpc * (p_hat * (1 - p_hat)) / (n - 1))
  moe_total <- (ci_upr_total - ci_lwr_total) / 2
  # output table
  out <- tibble::tibble(`Parameter` = c("Population Proportion", "Population Total"), 
                        `Estimate` = c(p_hat, total), 
                        `Std. Error` = c(sd_prop, sd_total), 
                        `MoE` = c(moe_prop, moe_total))
  # add rmoe
  if (show_rmoe) {
    rmoe_p_hat <- (moe_prop / p_hat) * 100
    rmoe_total <- (moe_total / total) * 100
    out <- dplyr::bind_cols(out, `RMoE (%)` = c(rmoe_p_hat, rmoe_total))
  }
  # add ci
  if (show_ci) {
    out <- dplyr::bind_cols(out, 
                            `Lower Bound` = c(ci_lwr_prop, ci_lwr_total), 
                            `Upper Bound` = c(ci_upr_prop, ci_upr_total))
  }
  # create html table
  out %>% 
    kable_html(digits = input$digits43, rowname_width = "250px")
}

### end ch4app3 server ###
