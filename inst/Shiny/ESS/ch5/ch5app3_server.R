### ch5app3 server ###

# number of strata
output$select_num_strat_53c <- renderUI({
  selectInput("num_strat_53c", "Number of Strata", 
              choices = c("2" = 2L, "3" = 3L, "4" = 4L, "5" = 5L, "6" = 6L, "7" = 7L, "8" = 8L, "9" = 9L), 
              selected = 2L)
})
# var or sd
output$var_or_sd_53c <- renderUI({
  radioButtons("var_or_sd_53c", "Variance or Standard Deviation", 
               choices = c("Variance" = 'var53',"Standard Deviation" = 'sd53'), selected = "sd53", inline = T)
})
# mean or total
output$mean_or_total_53c <- renderUI({
  radioButtons("mean_or_total_53c", "Parameter of Interest", 
               choices = c("Mean" = 'mean53', "Total" = 'total53'), inline = T)
})
# input mean bound 
output$select_Bmean53 <- renderUI({
  numericInput("Bmean53", "Desired Bound (Mean)", min = 0, max = Inf, value = NULL)
})
# input total bound 
output$select_Btotal53 <- renderUI({
  numericInput("Btotal53", "Desired Bound (Total)", min = 0, max = Inf, value = NULL)
})
# adjust mean bound 
output$adjust_BMean53 <- renderUI({
  req(input$Bmean53)
  B <- input$Bmean53
  min_value <- (B * 0.1) %>% round(digits = get_digits(B))
  max_value <- (B * 5) %>% round(digits = get_digits(B))
  step_value <- ((B * 5 - B * .1) / 100) %>% round(digits = get_digits(B))
  sliderInput("BMean53adj", "Adjust Bound (Mean)", value = B, 
              min = min_value, max = max_value, step = step_value, ticks = F, sep = "")
})
# adjust total bound 
output$adjust_BTotal53 <- renderUI({
  req(input$Btotal53)
  B <- input$Btotal53
  min_value <- (B * 0.1) %>% round(digits = get_digits(B))
  max_value <- (B * 5) %>% round(digits = get_digits(B))
  step_value <- ((B * 5 - B * .1) / 100) %>% round(digits = get_digits(B))
  sliderInput("BTotal53adj", "Adjust Bound (Total)", value = B, 
              min = min_value, max = max_value, step = step_value, ticks = F, sep = "")
})
# optimize N
output$opt_N53 <- renderUI({
  radioButtons("opt_N53", "Optimize Total Sample Size", 
               choices = c("Yes" = 'yes53', "No" = 'no53'), selected = "yes53", inline = T)
})
# select N
output$select_N53 <- renderUI({
  req(input$opt_N53, input$num_strat_53c)
  numericInput("select_N53", "Total Sample Size", min = input$num_strat_53c, max = Inf, value = NULL)
})

# user values ----

# stratum 1 
output$select_var1_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance1_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd1_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd1_53c", NULL, min = 0, max = Inf,value = NULL)})
output$select_c1_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c1_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N1_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N1_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 2
output$select_var2_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance2_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd2_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd2_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c2_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c2_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N2_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N2_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 3 
output$select_var3_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance3_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd3_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd3_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c3_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c3_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N3_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N3_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 4 
output$select_var4_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance4_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd4_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd4_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c4_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c4_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N4_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N4_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 5 
output$select_var5_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance5_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd5_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd5_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c5_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c5_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N5_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N5_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 6 
output$select_var6_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance6_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd6_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd6_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c6_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c6_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N6_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N6_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 7 
output$select_var7_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance7_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd7_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd7_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c7_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c7_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N7_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N7_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 8 
output$select_var8_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance8_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd8_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd8_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c8_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c8_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N8_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N8_53c",NULL, min = 2, max = Inf, value = NULL)})
# stratum 9 
output$select_var9_53c <- renderUI({req(input$num_strat_53c)
  numericInput("variance9_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_sd9_53c <- renderUI({req(input$num_strat_53c)
  numericInput("sd9_53c", NULL, min = 0, max = Inf, value = NULL)})
output$select_c9_53c <- renderUI({req(input$num_strat_53c)
  numericInput("c9_53c", NULL, min = 0, max = Inf, value = 1)})
output$select_N9_53c <- renderUI({req(input$num_strat_53c)
  numericInput("N9_53c",NULL, min = 2, max = Inf, value = NULL)})

# output options ----

# number of digits to display
output$digits53 <- renderUI({
  sliderInput("digits53", "Number of Decimals", value = 2, min = 0, max = 10, ticks = F, sep = "")
})

# output table ----
output$output_table53 <- function() {
  req(input$digits53, input$var_or_sd_53c, input$num_strat_53c, input$opt_N53, input$mean_or_total_53c)
  k <- input$num_strat_53c; sd <- numeric(); N <- numeric(); c <- numeric(); mean_or_total <- input$mean_or_total_53c
  if (mean_or_total == "mean53") {req(input$BMean53adj)} else{req(input$BTotal53adj)}
  # inputs 
  if (k >= 1) {req(input$c1_53c, input$N1_53c); N <- c(N, input$N1_53c); c <- c(c, input$c1_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd1_53c); sd <- c(sd, input$sd1_53c)} 
    else {req(input$variance1_53c); sd <- c(sd, sqrt(input$variance1_53c))}}
  if (k >= 2) {req(input$c2_53c, input$N2_53c); N <- c(N, input$N2_53c); c <- c(c, input$c2_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd2_53c); sd <- c(sd, input$sd2_53c)} 
    else {req(input$variance2_53c); sd <- c(sd, sqrt(input$variance2_53c))}}
  if (k >= 3) {req(input$c3_53c, input$N3_53c); N <- c(N, input$N3_53c); c <- c(c, input$c3_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd3_53c); sd <- c(sd, input$sd3_53c)
    } else {req(input$variance3_53c); sd <- c(sd, sqrt(input$variance3_53c))}}
  if (k >= 4) {req(input$c4_53c, input$N4_53c); N <- c(N, input$N4_53c); c <- c(c, input$c4_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd4_53c); sd <- c(sd, input$sd4_53c)
    } else {req(input$variance4_53c); sd <- c(sd, sqrt(input$variance4_53c))}}
  if (k >= 5) {req(input$c5_53c, input$N5_53c); N <- c(N, input$N5_53c); c <- c(c, input$c5_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd5_53c); sd <- c(sd, input$sd5_53c)
    } else {req(input$variance5_53c); sd <- c(sd, sqrt(input$variance5_53c))}}
  if (k >= 6) {req(input$c6_53c, input$N6_53c); N <- c(N, input$N6_53c); c <- c(c, input$c6_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd6_53c); sd <- c(sd, input$sd6_53c)
    } else {req(input$variance6_53c); sd <- c(sd, sqrt(input$variance6_53c))}}
  if (k >= 7) {req(input$c7_53c, input$N7_53c); N <- c(N, input$N7_53c); c <- c(c, input$c7_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd7_53c); sd <- c(sd, input$sd7_53c)
    } else {req(input$variance7_53c); sd <- c(sd, sqrt(input$variance7_53c))}}
  if (k >= 8) {req(input$c8_53c, input$N8_53c); N <- c(N, input$N8_53c); c <- c(c, input$c8_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd8_53c); sd <- c(sd, input$sd8_53c)
    } else {req(input$variance8_53c); sd <- c(sd, sqrt(input$variance8_53c))}}
  if (k >= 9) {req(input$c9_53c, input$N9_53c); N <- c(N, input$N9_53c); c <- c(c, input$c9_53c)
    if (input$var_or_sd_53c == "sd53") {req(input$sd9_53c); sd <- c(sd, input$sd9_53c)
    } else {req(input$variance9_53c); sd <- c(sd, sqrt(input$variance9_53c))}}
  # calculations
  c <- c / sum(c); N_pop <- sum(N)
  if (mean_or_total == "mean53") {
    B <- input$BMean53adj
    D <- B^2 / 4
    if (input$opt_N53 == "no53") {
      n_all_mean <- input$select_N53
    } else {
      n_all_mean <- sum(N * sd / sqrt(c)) * sum(N * sd * sqrt(c)) / ((N_pop^2 * D) + sum(N * sd^2))
    }
    n_mean_i <- n_all_mean * ((N * sd / sqrt(c)) / (sum(N * sd / sqrt(c))))
  } else {
    B <- input$BTotal53adj
    D <- B^2 / (4 * (N_pop^2))
    if (input$opt_N53 == "no53") {
      n_all_total <- input$select_N53
    } else {
      n_all_total <- sum(N * sd / sqrt(c)) * sum(N * sd * sqrt(c)) / ((N_pop^2 * D) + sum(N * sd^2))
    }
    n_total_i <- n_all_total * ((N * sd / sqrt(c)) / (sum(N * sd / sqrt(c))))
  }
  # output table
  label <- character()
  for (i in 1:length(N)) {label[i] <- paste("Stratum ", i, sep = " ")}
  if (input$opt_N53 == "no53") {
    if (mean_or_total == "mean53") {out <- tibble(`Strata ID` = c(label), `Sample Size` = c(n_mean_i))
    } else {out <- tibble(`Strata ID` = c(label), `Sample Size` = c(n_total_i))}
  } else {
    if (mean_or_total == "mean53") {
      out <- tibble(` ` = c("N Required to Estimate Total", "Optimal Allocations by Strata", label),
                    `  ` = c(n_all_mean, "--------------------", n_mean_i))
    } else {
      out <- tibble(` ` = c("N Required to Estimate Total", "Optimal Allocations by Strata", label),
                    `  ` = c(n_all_total, "--------------------", n_total_i))
    }
  }
  # create html table
  out %>% 
    kable_html(digits = input$digits53, caption = "Optimal Allocations", rowname_width = "400px")
}

### end ch5app3 server ###